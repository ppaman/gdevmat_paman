void setup()
{
  size(1920,1080,P3D);
  camera(0,0,-(height/2)/tan(PI*30/180),//camera position
        0,0,0, //eye position
        0,-1,0); //up vector
}

int y;

void draw()
{
  background(255,204,0);
  drawCartesianPlane();
  drawLinearFunction();
  drawQuadraticFunction();
  drawCircle();
  drawSine();
}

void drawCartesianPlane()
{
    stroke(0, 0, 0);
    line(300, 0, -300, 0);
    line(0,300,0,-300);
    strokeWeight(3);
    
    for(int i = -300; i<= 300; i+=10)
    {
      line(i, -5, i, 5);
      line(-5, i ,5, i);
    }
}

void drawLinearFunction()
{
  for(int x = -200;x<=200;x++)
  {
    circle(x,x+2,1);
  }
}

void drawQuadraticFunction()
{
  for(float x = -300;x<=300;x+=0.1)
  {
    circle(x*10,(float)Math.pow(x,2)+(2*x)-5,1);
  }
}

float radius = 30;
void drawCircle()
{
  for(int x = 0;x<360;x++)
  {
    circle((float)Math.cos(x)*radius,(float)Math.sin(x)*radius,1);
  }
}

float amplitude = 50;
float frequency;
float f(float x)
{
  x = 0;
  return (float)amplitude*sin(x*frequency);
}

void drawSine()
{
  strokeWeight(3);
  stroke(255, 255, 255);
  for(float i = -300;i<300;i++)
  {
     line(i,f(i),i+1,f(i+1));
  }
}
