class Walker
{
  float xPos;
  float yPos;
  
  void render()
  {
    noStroke();
    fill(random(255),random(255),random(255),100.0);
    circle(xPos,yPos,30);
  }
  
  void randomWalk()
  {
    int decision = floor(random(8));
    if(decision==0)
    {
      yPos+=5;
    }
    else if(decision==1)
    {
      yPos-=5;
    }
    else if(decision==2)
    {
      xPos+=5;
    }
    else if(decision==3)
    {
      xPos-=5;
    }
    else if(decision==4)
    {
      xPos+=5;
      yPos+=5;
    }
    else if(decision==5)
    {
      xPos+=5;
      yPos-=5;
    }
    else if(decision==6)
    {
      xPos-=5;
      yPos-=5;
    }
    else if(decision==7)
    {
      xPos-=5;
      yPos+=5;
    }
  }
}
